# InterNations

This project was developed as part of a test proposed by InterNations for a 
Front-End developer oportunity

The project was developed with:
- **Back-End** = Laravel 6.0
- **Front-End** = React 16.9

## Goals

Bellow is a list of primaries and secondaries features requested

**Primary**
- [x] I can see a list of existing users
- [x] I can see a list of existing groups
- [x] I can create users
- [x] I can create groups
- [x] I can assign users to a group they aren’t already part of
- [x] I can remove users from a group
- [x] I can delete groups when they no longer have members
- [x] I can delete users

**Secondary**
- [x] A "user detail page" where it is possible to see a list of all groups that a given user is in
- [x] • A "group detail page" where it is possible to see a list of all users in a given group
- [x] • Search functionality
- [x] • When a user is deleted, he is removed from all groups he belonged to
- [x] • A user cannot exist without having at least one group (think validation when creating a user)
- [x] • Input validation on the client-side
- [x] • Make it responsive!
- [ ] • JS tests :cry:
- [x] • ES6 syntax!
- [x] • ESLint (with some custom rules)
- [ ] • Offline capabilities :cry:
- [x] • Anything else you think it’s cool!

## Getting Started

These instructions will get you a copy of the project up and running on your local machine. 

### Prerequisites
- **Back-End**
- PHP >= 7.2.0
- BCMath PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Composer

- **Front-End**
- npm or yarn

You can also visit the [Laravel Documentation website](https://laravel.com/docs/) for more information.


### Installing

1. Onde you took care of the requeriments, lets install the software.
2. Clone this repository to your computer, and access the folder you chose
3. lets firts take care of the Front-End
4. from the main folder access the "front" folder
5. once in the "front" folder install the packages (npm i * *or* * yarn)
6. then go back to the main folder. a "cd .." always help.
7. from the main folder access the "back" folder
8. once inside lets install it!
9. run "composer install"
10. then rename the ".env.example" to ".env"
11. onde that is done run "php artisan key:generate".
12. then in the database folder create an empty file called "database.sqlite"
13. the next one is "php artisan migrate" from the main folder. This will generate our sqlite database.
14. That is it. Now lets run it.
15. still on the folder run "php artisan serve" this will run the backend on localhost:8000 port;
16. in case this port is busy, the system will pick another one and inform you. 
17. if it is different you need to set the front-end. Change the url on "/front/src/services/api.js"
18. the last step is to run the front-end. To do so from the "front" folder run "yarn start"
19. now open your browser and enjoy :satisfied:

## Important
After installing and trying to run the system locally a CORS issue may happen;
A possible workaround would be installing the "Allow-Control-Allow-Origin: *" extension for chrome
This extra step helps you running the system locally.

## API

### Endpoints

The system has 2 main features, which are "Users" and "Groups"

for wild cards I will us :parameter

**User Endpoints**
```
GET | /api/users -- get all users registered
GET | /api/users/:id -- get a specific users data
POST| /api/users -- create a new user
PUT | /api/users/:id -- update data from a specific user
DELETE | /api/users/:id -- deletes a user from system
```

**Group Endpoints**
```
GET | /api/groups -- get all groups registered
GET | /api/groups/:id -- get a specific group data
POST| /api/groups -- create a new group
PUT | /api/groups/:id -- update data from a specific group
DELETE | /api/groups/:id -- deletes a group from system
```

**Sucess response**
- User structure
```
{
"data": [
    {
        "id": 1,
        "name": "Username",
        "email": "email@user.com",
        "mobile": "+55 555 55555",
        "created_at": "2019-09-27 04:42:05",
        "updated_at": "2019-09-27 04:42:15",
        "groups": [
            {
                "id": 4,
                "name": "Group Name",
                "description": "Group description",
                "created_at": "2019-09-27 04:43:24",
                "updated_at": "2019-09-27 04:43:24",
                "pivot": {
                    "user_id": "1",
                    "group_id": "4"
                }
            }
        ] 
    }
]
}
```
- Grouo structure
```
{
"data": [
    {
      "id": 4,
      "name": "Group name",
      "description": "Group description",
      "created_at": "2019-09-27 04:43:24",
      "updated_at": "2019-09-27 04:43:24",
      "users": [
        {
          "id": 1,
          "name": "User name",
          "email": "email@user.com",
          "mobile": "+55 555 5555",
          "created_at": "2019-09-27 04:42:05",
          "updated_at": "2019-09-27 04:42:15",
          "pivot": {
            "group_id": "4",
            "user_id": "1"
          }
        }
      ]
    }
]
}
```

**Genéric Error response**
```
{
  "error": "There is no instance of user with the given id",
  "code": 404
}
```

**Validation Error response**
```
{
  "errors": [
      [name=> "field name is required"],
      [email=> "field email should be a valid email"],
  ], 
  "code": 422
}
```

## Built With

* [Laravel](https://laravel.com) - PHP Framework
* [React](https://pt-br.reactjs.org/) - Javascript Framework
* [Styled-Components](https://www.styled-components.com/) - Sass in JS files
* [Ant.Design](https://www.styled-components.com/) - A React / Css Framework


I Hope you guys like it, so I can join the team :)

### Thats all Folks
