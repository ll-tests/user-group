<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = "groups";
    protected $fillable = [
        'name', 'description'
    ];

    protected $casts = [
        'users'
    ];

    public function users()
    {
        return $this->belongsToMany("App\User");
    }
}
