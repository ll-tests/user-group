<?php

namespace App\Http\Controllers;

use App\User;
use App\Group;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GroupController extends Controller
{
    use ApiResponser;

    public function index()
    {
        $groups = Group::with('users')->get();
        return $this->successResponse($groups);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'description' => 'required|max:255'
        ];

        $this->validate($request, $rules);

        $group = Group::create($request->all());
        if (!empty($request->users)) {
            $group->users()->sync($request->users);
        }

        return $this->successResponse($group);
    }

    public function show($group)
    {
        $group = Group::with('users')->findOrFail($group);
        return $this->successResponse($group);
    }

    public function update(Request $request, $group)
    {
        $request->validate([
            'name'=> 'max:255',
            'description'=> 'max:255',
            'users'=>''
        ]);

        $group = Group::findOrFail($group);
        $group->update($request->all());

        $removedUsers = $this->checkUsersRemoved($group, $request->users);
        if ($removedUsers !== true) {
            return $this->errorResponse($removedUsers, Response::HTTP_UNAUTHORIZED);
        }

        $group->users()->sync($request->users);

        return $this->successResponse($group);
    }

    public function destroy($group)
    {
        $group = Group::findOrFail($group);
        if ($group->users->count() > 0) {
            return $this->errorResponse(
                'Unable to delete groups whit participants on it.',
                Response::HTTP_UNAUTHORIZED
            );
        }
        $group->delete();
        return $this->successResponse($group);
    }

    public function checkUsersRemoved($group, $users)
    {
        $invalid= [];
        $users = isset($users) && !empty($users) ? $users : [];
        foreach ($group->users as $user) {
            if (!in_array($user->id, $users)) {
                if ($user->groups->count() <= 1) {
                    array_push($invalid, $user->name);
                }
            }
        }

        if (count($invalid) > 0) {
            $user = implode(', ', $invalid);
            return 'Users ('.$user.') can not be removed. They need to be in one group at least';
        }

        return true;
    }
}
