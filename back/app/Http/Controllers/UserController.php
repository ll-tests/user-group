<?php

namespace App\Http\Controllers;

use App\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    use ApiResponser;

    public function index()
    {
        $users = User::with('groups')->get();
        return $this->successResponse($users);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'mobile' => 'max:255',
            'groups' => 'min:1'
        ];

        $this->validate($request, $rules, [
            'groups.min' => 'The User should belong to 1 group at least'
        ]);

        $user = User::create($request->all());
        if (!empty($request->groups)) {
            $user->groups()->sync($request->groups);
        }

        return $this->successResponse($user);
    }

    public function show($user)
    {
        $user = User::with('groups')->findOrFail($user);
        return $this->successResponse($user);
    }

    public function update(Request $request, $user)
    {
        $rules = [
            'name' => 'sometimes|required|max:255',
            'email' => 'sometimes|required|max:255',
            'mobile' => 'sometimes|max:255',
            'groups' => 'min:1'
        ];

        $this->validate($request, $rules);

        $user = User::findOrFail($user);
        $user->update($request->all());

        $user->groups()->sync($request->groups);

        return $this->successResponse($user);
    }

    public function destroy($user)
    {
        $user = User::findOrFail($user);
        $user->delete();
        return $this->successResponse($user);
    }
}
