<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = "users";
    protected $fillable = [
        'name', 'email', 'mobile'
    ];

    protected $casts =["groups"=>'array'];

    public function groups()
    {
        return $this->belongsToMany("App\Group");
    }
}
