<?php

use Illuminate\Http\Request;

Route::prefix('groups')->group(function () {
    Route::get('/', 'GroupController@index');
    Route::post('/', 'GroupController@store');
    Route::get('/{group}', 'GroupController@show');
    Route::put('/{group}', 'GroupController@update');
    Route::delete('/{group}', 'GroupController@destroy');
    Route::delete('/{groups}/{user}/remove', 'GroupController@removeUser');
});

Route::prefix('users')->group(function () {
    Route::get('/', 'UserController@index');
    Route::post('/', 'UserController@store');
    Route::get('/{user}', 'UserController@show');
    Route::put('/{user}', 'UserController@update');
    Route::delete('/{user}', 'UserController@destroy');
});
