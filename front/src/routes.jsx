import React from "react";
import { Router, Switch, Route } from "react-router-dom";
import history from "./history";

import Home from "./pages/Home";
import UserList from "./pages/Users/List";
import UserCreate from "./pages/Users/Create";
import UserEdit from "./pages/Users/Edit";

import GroupList from "./pages/Groups/List";
import GroupCreate from "./pages/Groups/Create";
import GroupEdit from "./pages/Groups/Edit";

export default function Routes() {
  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/users" component={UserList} />
        <Route exact path="/users/create" component={UserCreate} />
        <Route exact path="/users/:id" component={UserEdit} />

        <Route exact path="/groups" component={GroupList} />
        <Route exact path="/groups/create" component={GroupCreate} />
        <Route exact path="/groups/:id" component={GroupEdit} />
      </Switch>
    </Router>
  );
}
