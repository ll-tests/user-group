import React from 'react';
import Routes from './routes';

import 'antd/dist/antd.css';
import './assets/styles/app.scss';

function App() {
	return <Routes />;
}

export default App;
