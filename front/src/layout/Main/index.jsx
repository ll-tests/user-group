import React from 'react';
import { Icon, Button, Spin } from 'antd';
import { isString } from 'is-what';
import history from '../../history';

import { Header, Main } from './styles';

export default function layout(props) {
	const {
		title,
		children,
		loading = false,
		showLinkLeft = false,
		showLinkRight = false,
		linkLeft = null,
		linkRight = null
	} = props;

	const handleLinkLeft = () => {
		if (isString(linkLeft)) {
			history.push(`${linkLeft}`);
		} else {
			linkLeft();
		}
	};

	const handleLinkRight = () => {
		if (isString(linkRight)) {
			history.push(`${linkRight}`);
		} else {
			linkRight();
		}
	};

	return (
		<Spin spinning={loading != undefined && loading == true}>
			<Header>
				<div className="container">
					{showLinkLeft && (
						<Button type="link" className="return-link" onClick={handleLinkLeft}>
							<Icon type="left" />
						</Button>
					)}
					{title}
					{showLinkRight && (
						<Button type="link" className="add-link" onClick={handleLinkRight}>
							<Icon type="plus" />
						</Button>
					)}
				</div>
			</Header>
			<Main>{children}</Main>
		</Spin>
	);
}
