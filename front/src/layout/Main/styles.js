import styled from 'styled-components';

export const Header = styled.header `
  position: relative;
  display: flex;
  width: 100%;
  height: 60px;
  background-color: #559CE4;
  justify-content: center;
  align-items: center;
  font-size: 1.5em;
  font-weight: bold;
  color: #fff;
  margin-bottom: 20px;

  .container {
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    max-width: 1024px;
    margin: 0 auto;
  }

  .return-link {
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0;
    padding: 0;
    font-size: 21px;
    top: 0;
    left: 0;
    height: 100%;
    width: 60px;
    color: #fff;

    &:hover {
      color: #dd0;
    }
  }

  .add-link {
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0;
    padding: 0;
    font-size: 21px;
    top: 0;
    right: 0;
    height: 100%;
    width: 60px;
    color: #fff;

    &:hover {
      color: #dd0;
    }
  }
`;

export const Main = styled.main `
  display: flex;
  flex-direction: column;
  flex: 1 1;
  width: 100%;
  max-width: 1024px;
  margin: 0 auto;
  justify-content: center;
  font-size: 1em;
  padding: 0 20px;
  
  @media (min-width: 1045px) {
    padding: 0;
  }
`;