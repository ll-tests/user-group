import React, { useEffect, useState } from 'react';
import { Icon, Modal, notification } from 'antd';

import Bar from '../../../components/Bar';
import Main from '../../../layout/Main';
import api from '../../../services/api';
import history from '../../../history';

import DataList from '../../../components/DataList';

export default function List() {
	const [ groupList, setGroupList ] = useState([]);
	const [ filteredList, setFilteredList ] = useState([]);
	const [ pageLoading, setPageLoading ] = useState(false);
	const [ searchValue, setSearchValue ] = useState(null);

	const fetchGroups = async () => {
		try {
			const response = await api.get('/groups');
			const { data: { data } } = response;
			setGroupList(data);
			return;
		} catch ({ response }) {
			if (response) {
				notification.error({ message: response.data.error });
			} else {
				notification.error({ message: 'Unexpected Error!' });
			}
		}
	};

	const filterGroupList = (search) => {
		setSearchValue(search);
	};

	useEffect(() => {
		setPageLoading(true);
		fetchGroups();
		setPageLoading(false);
	}, []);

	useEffect(
		() => {
			if (searchValue) {
				const filter = groupList.filter((group) => {
					return group.name.toLowerCase().indexOf(searchValue.toLowerCase()) > -1;
				});
				setFilteredList(filter);
			} else {
				setFilteredList(groupList);
			}
		},
		[ searchValue, groupList ]
	);

	const deleteGroup = async (group) => {
		setPageLoading(true);
		try {
			await api.delete(`/groups/${group}`);
			notification.success({ message: 'Group deleted' });
			fetchGroups();
		} catch ({ response }) {
			if (response) {
				notification.error({ message: response.data.error });
			} else {
				notification.error({ message: 'Unexpected Error!' });
			}
		}
		setPageLoading(false);
	};

	const confirmDelete = async (group) => {
		Modal.confirm({
			title: 'Are you sure delete this Group?',
			content: "You can't undo this",
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'Cancel',
			onOk() {
				deleteGroup(group);
			}
		});
	};

	const editGroup = (group) => {
		history.push(`groups/${group.id}`);
	};

	const columns = [
		{
			title: 'Name',
			dataIndex: 'name',
			render: (value, row) => {
				return (
					<div>
						<button
							type="button"
							onClick={(e) => {
								e.stopPropagation();
								confirmDelete(row.id);
							}}
							className="delete-link"
						>
							<Icon type="delete" />
						</button>
						<p className="title">{row.name}</p>
						<span className="subtitle">
							<strong>Description: </strong>
							{row.description}
						</span>
					</div>
				);
			}
		}
	];

	return (
		<Main title="Groups" showLinkLeft linkLeft="/" showLinkRight linkRight="/groups/create">
			<Bar onSearch={filterGroupList} />
			<DataList data={filteredList} columns={columns} loading={pageLoading} openLink={editGroup} />
		</Main>
	);
}
