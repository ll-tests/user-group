import React, { useState } from 'react';
import { Col, Row, notification } from 'antd';
import { Formik } from 'formik';
import * as Yup from 'yup';
import history from '../../../history';
import api from '../../../services/api';

import Main from '../../../layout/Main';
import Box from '../../../components/Box';
import FormStyle from '../../../components/Form';
import GroupForm from '../Form';

const SignupSchema = Yup.object().shape({
	name: Yup.string().max(255, 'Too Long!').required('Required'),
	description: Yup.string().min(2, 'Too Short!').required('Required')
});

export default function Create() {
	const [ pageLoading, setPageLoading ] = useState(false);

	const saveForm = async (values, setErrors, setSubmitting) => {
		setPageLoading(true);
		setSubmitting(true);
		try {
			await api.post('/groups', values);
			notification.success({ message: 'User created' });
			history.push('/groups');
		} catch ({ response }) {
			if (response) {
				if (response.status != undefined && response.status == 422) {
					setErrors(response.data.error);
					notification.warning({
						message: 'Please, fill all the fields correctly'
					});
				} else {
					notification.error({ message: response.data.error });
				}
			} else {
				notification.error({ message: 'Unexpected Error!' });
			}
		}
		setSubmitting(false);
		setPageLoading(false);
	};

	return (
		<Main loading={pageLoading} title="Groups" showLinkLeft linkLeft="/groups">
			<Row gutter={16} type="flex" justify="space-around">
				<Col xl={10} md={16} sm={18} xs={22}>
					<Box>
						<FormStyle />
						<Formik
							initialValues={{
								name: '',
								description: ''
							}}
							validationSchema={SignupSchema}
							onSubmit={(values, { setErrors, setSubmitting }) => {
								saveForm(values, setErrors, setSubmitting);
								setSubmitting(false);
							}}
						>
							{() => <GroupForm />}
						</Formik>
					</Box>
				</Col>
			</Row>
		</Main>
	);
}
