import React, { useEffect, useState } from "react";
import { ErrorMessage } from "formik";
import { notification } from "antd";
import { Form, Input, Select, SubmitButton } from "@jbuschke/formik-antd";
import api from "../../../services/api";

import FormStyle from "../../../components/Form";

export default function UserForm() {
  const [userList, setUserList] = useState([]);

  const fetchUsers = async () => {
    try {
      const {
        data: { data }
      } = await api.get("/users");
      setUserList(data);
    } catch ({ response }) {
      if (response) {
				notification.error({ message: response.data.error });
			} else {
				notification.error({ message: 'Unexpected Error!' });
			}
    }
  };

  const { Option } = Select;

  useEffect(() => {
    fetchUsers();
  }, []);

  const renderGroupList = () => {
    return userList.map(user => {
      return (
        <Option key={user.id} value={user.id}>
          {user.name}
        </Option>
      );
    });
  };

  return (
    <>
      <FormStyle />
      <Form>
        <label htmlFor="name">
          <span>Name</span>
          <Input name="name" />
          <div className="input-error-msg">
            <ErrorMessage name="name" className="input-error-msg" />
          </div>
        </label>

        <label htmlFor="description">
          <span>Description</span>
          <Input name="description" />
          <div className="input-error-msg">
            <ErrorMessage name="description" className="input-error-msg" />
          </div>
        </label>

        <label htmlFor="users">
          <span>Users</span>
          <Select name="users" mode="multiple">
            {renderGroupList()}
          </Select>
          <ErrorMessage name="users" />
        </label>

        <div className="form-actions">
          <SubmitButton htmlType="submit" type="primary">
            Save
          </SubmitButton>
        </div>
      </Form>
    </>
  );
}
