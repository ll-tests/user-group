import React, { useState, useEffect } from 'react';
import { Col, Row, notification } from 'antd';
import { Formik } from 'formik';
import * as Yup from 'yup';
import history from '../../../history';
import api from '../../../services/api';

import Main from '../../../layout/Main';
import Box from '../../../components/Box';
import FormStyle from '../../../components/Form';
import GroupForm from '../Form';

const SignupSchema = Yup.object().shape({
	name: Yup.string().max(255, 'Too Long!').required('Required'),
	description: Yup.string().min(2, 'Too Short!').required('Required')
});

export default function Create(props) {
	const [ formData, setFormData ] = useState({});
	const [ pageLoading, setPageLoading ] = useState(false);

	const saveGroup = async (values, setErrors, setSubmitting) => {
		setPageLoading(true);
		setSubmitting(true);
		try {
			await api.put(`/groups/${formData.id}`, values);
			notification.success({ message: 'Group updated' });
			history.push('/groups');
		} catch (error) {
			const { response } = error;

			if (response.status === 422) {
				setErrors(response.data.error);
				notification.warning({ message: 'Check your data' });
			} else {
				notification.warning({ message: response.data.error });
			}
		}
		setSubmitting(false);
		setPageLoading(false);
	};

	const fetchGroup = async () => {
		setPageLoading(true);
		try {
			const { match: { params } } = props;

			const response = await api.get(`/groups/${params.id}`);
			const { data: { data } } = response;

			data.users = data.users.map((user) => {
				return user.id;
			});

			setFormData(data);
			setPageLoading(false);
			return;
		} catch ({ response }) {
			notification.error({ message: response.data.error });
		}
		setPageLoading(false);
	};

	useEffect(() => {
		const fetch = () => {
			fetchGroup();
		};

		fetch();
	}, []);

	return (
		<Main title="Groups" showLinkLeft linkLeft="/groups" loading={pageLoading}>
			<Row gutter={16} type="flex" justify="space-around">
				<Col xl={10} md={16} sm={18} xs={22}>
					<Box>
						<FormStyle />
						<Formik
							validationSchema={SignupSchema}
							enableReinitialize
							initialValues={formData}
							onSubmit={(values, { setErrors, setSubmitting }) => {
								saveGroup(values, setErrors, setSubmitting);
								setSubmitting(false);
							}}
						>
							{() => <GroupForm />}
						</Formik>
					</Box>
				</Col>
			</Row>
		</Main>
	);
}
