import React, { useEffect, useState } from 'react';
import { Icon, Modal, notification } from 'antd';

import Bar from '../../../components/Bar';
import Main from '../../../layout/Main';
import api from '../../../services/api';
import history from '../../../history';

import DataList from '../../../components/DataList';

export default function List() {
	const [ users, setUsers ] = useState([]);
	const [ filteredList, setFilteredList ] = useState([]);
	const [ pageLoading, setPageLoading ] = useState(false);
	const [ searchValue, setSearchValue ] = useState(null);

	// fetch users to state
	const fetchUsers = async () => {
		setPageLoading(true);
		try {
			const response = await api.get('/users');
			const { data: { data } } = response;
			setUsers(data);
		} catch ({ response }) {
			if (response) {
				notification.error({ message: response.data.error });
			} else {
				notification.error({ message: 'Unexpected Error!' });
			}
		}
		setPageLoading(false);
	};

	const filterUserList = (search) => {
		setSearchValue(search);
	};

	// on component did mount
	useEffect(() => {
		fetchUsers();
	}, []);

	useEffect(
		() => {
			if (searchValue) {
				const filter = users.filter((user) => {
					return user.name.toLowerCase().indexOf(searchValue.toLowerCase()) > -1;
				});
				setFilteredList(filter);
			} else {
				setFilteredList(users);
			}
		},
		[ searchValue, users ]
	);

	const deleteUser = async (user) => {
		setPageLoading(true);
		try {
			const response = await api.delete(`/users/${user}`);
			const { status } = response;
			if (status !== 200) {
				notification.error({ message: 'An error ocurred!' });
			} else {
				notification.success({ message: 'User deleted!' });
			}
			fetchUsers();
		} catch ({ response }) {
			if (response) {
				notification.error({ message: response.data.error });
			} else {
				notification.error({ message: 'Unexpected Error!' });
			}
		}
		setPageLoading(false);
	};

	const confirmDelete = async (user) => {
		Modal.confirm({
			title: 'Are you sure delete this User?',
			content: "You can't undo this",
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'Cancel',
			onOk() {
				deleteUser(user);
			}
		});
	};

	const editUser = (user) => {
		history.push(`users/${user.id}`);
	};

	const columns = [
		{
			title: 'Name',
			dataIndex: 'name',
			render: (value, row) => {
				return (
					<div>
						<button
							type="button"
							onClick={(e) => {
								e.stopPropagation();
								confirmDelete(row.id);
							}}
							className="delete-link"
						>
							<Icon type="delete" />
						</button>
						<p className="title">{row.name}</p>
						<span className="subtitle">
							<strong>E-mail:</strong>
							{row.email}
						</span>
						<span className="subtitle">
							<strong>Mobile:</strong>
							{row.mobile}
						</span>
					</div>
				);
			}
		}
	];

	return (
		<Main title="Users" showLinkLeft linkLeft="/" showLinkRight linkRight="/users/create">
			<Bar onSearch={filterUserList} />
			<DataList data={filteredList} columns={columns} loading={pageLoading} openLink={editUser} />
		</Main>
	);
}
