import React, { useState } from 'react';
import { Col, Row, notification } from 'antd';
import { Formik } from 'formik';
import * as Yup from 'yup';
import history from '../../../history';
import api from '../../../services/api';

import Main from '../../../layout/Main';
import Box from '../../../components/Box';
import FormStyle from '../../../components/Form';
import UserForm from '../Form';

const SignupSchema = Yup.object().shape({
	name: Yup.string().min(2).max(255, 'Too Long!').required('Required'),
	email: Yup.string().min(2, 'Too Short!').email().max(255, 'Too Long!').required('Required'),
	groups: Yup.array().min(1, 'Users should be in 1 group at least')
});

export default function Create() {
	const [ pageLoading, setPageLoading ] = useState(false);

	const saveUser = async (values, setErrors, setSubmitting) => {
		setPageLoading(true);
		setSubmitting(true);
		try {
			await api.post('/users', values);
			notification.success({ message: 'User created' });
			history.push('/users');
		} catch (error) {
			const { response } = error;

			if (response) {
				if (response.status === 422) {
					setErrors(response.data.errors);
					notification.warning({ message: 'Check your data' });
				} else if (response.status == 500) {
					notification.warning({ message: 'Unexpected Error, Try later' });
				}
			} else {
				notification.error({ message: 'Unexpected Error!' });
			}
		}
		setSubmitting(false);
		setPageLoading(false);
	};

	return (
		<Main loading={pageLoading} title="Users" showLinkLeft linkLeft="/users">
			<Row gutter={16} type="flex" justify="space-around">
				<Col xl={10} md={16} sm={18} xs={22}>
					<Box>
						<FormStyle />
						<Formik
							validationSchema={SignupSchema}
							initialValues={{
								name: '',
								email: '',
								mobile: '',
								groups: []
							}}
							onSubmit={(values, { setErrors, setSubmitting }) => {
								saveUser(values, setErrors, setSubmitting);
								setSubmitting(false);
							}}
						>
							{() => <UserForm />}
						</Formik>
					</Box>
				</Col>
			</Row>
		</Main>
	);
}
