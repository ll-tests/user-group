import React, { useEffect, useState } from "react";
import { ErrorMessage } from "formik";
import { notification } from "antd";
import { Form, Input, Select, SubmitButton } from "@jbuschke/formik-antd";
import FormStyle from "../../../components/Form";
import api from "../../../services/api";

export default function UserForm() {
  const [groupList, setGroupList] = useState([]);

  const fetchGroups = async () => {
    try {
      const {
        data: { data }
      } = await api.get("/groups");
      setGroupList(data);
    } catch ({ response }) {
      if (response) {
				notification.error({ message: response.data.error });
			} else {
				notification.error({ message: 'Unexpected Error!' });
			}
    }
  };

  const { Option } = Select;

  useEffect(() => {
    fetchGroups();
  }, []);

  const renderGroupList = () => {
    return groupList.map(group => {
      return (
        <Option key={group.id} value={group.id}>
          {group.name}
        </Option>
      );
    });
  };

  return (
    <>
      <FormStyle />
      <Form>
        <label htmlFor="name">
          <span>Name</span>
          <Input name="name" />
          <div className="input-error-msg">
            <ErrorMessage name="name" className="input-error-msg" />
          </div>
        </label>

        <label htmlFor="email">
          <span>E-mail</span>
          <Input name="email" />
          <div className="input-error-msg">
            <ErrorMessage name="email" className="input-error-msg" />
          </div>
        </label>

        <label htmlFor="mobile">
          <span>Mobile</span>
          <Input name="mobile" />

          <div className="input-error-msg">
            <ErrorMessage name="mobile" />
          </div>
        </label>

        <label htmlFor="groups">
          <span>Groups</span>
          <Select name="groups" mode="multiple">
            {renderGroupList()}
          </Select>
          <div className="input-error-msg">
            <ErrorMessage name="groups" className="input-error-msg" />
          </div>
        </label>

        <div className="form-actions">
          <SubmitButton htmlType="submit" type="primary">
            Save
          </SubmitButton>
        </div>
      </Form>
    </>
  );
}
