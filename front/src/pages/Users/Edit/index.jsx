import React, { useState, useEffect } from 'react';
import { Col, Row, notification } from 'antd';
import { Formik } from 'formik';
import * as Yup from 'yup';
import history from '../../../history';
import api from '../../../services/api';

import Main from '../../../layout/Main';
import Box from '../../../components/Box';
import FormStyle from '../../../components/Form';
import UserForm from '../Form';

const SignupSchema = Yup.object().shape({
	name: Yup.string().min(2).max(255, 'Too Long!').required('Required'),
	email: Yup.string().min(2, 'Too Short!').email().max(255, 'Too Long!').required('Required'),
	groups: Yup.array().min(1, 'Users should be in 1 group at least')
});

export default function Create(props) {
	const [ formData, setFormData ] = useState({});
	const [ pageLoading, setPageLoading ] = useState(false);

	const saveUser = async (values, setErrors, setSubmitting) => {
		setPageLoading(true);
		setSubmitting(true);
		try {
			await api.put(`/users/${formData.id}`, values);
			notification.success({ message: 'User Updated' });
			history.push('/users');
		} catch (error) {
			const { response } = error;

			if (response.status === 422) {
				setErrors(response.data.errors);
				notification.warning({ message: 'Check your data' });
			} else if (response.status == 500) {
				notification.warning({ message: 'Unexpected Error, Try later' });
			}
		}
		setSubmitting(false);
		setPageLoading(false);
	};

	const fetchUser = async () => {
		try {
			const { match: { params } } = props;

			const response = await api.get(`/users/${params.id}`);
			const { data: { data } } = response;

			data.groups = data.groups.map((group) => {
				return group.id;
			});

			setFormData(data);
			return;
		} catch ({ response }) {
			notification.error({ message: response.data.error });
		}
	};

	useEffect(() => {
		const fetch = () => {
			fetchUser();
		};

		fetch();
	}, []);

	return (
		<Main title="Users" showLinkLeft linkLeft="/users" loading={pageLoading}>
			<Row gutter={16} type="flex" justify="space-around">
				<Col xl={10} md={16} sm={18} xs={22}>
					<Box>
						<FormStyle />
						<Formik
							validationSchema={SignupSchema}
							enableReinitialize
							initialValues={formData}
							onSubmit={(values, { setErrors, setSubmitting }) => {
								saveUser(values, setErrors, setSubmitting);
								setSubmitting(false);
							}}
						>
							{({ touched }) => <UserForm />}
						</Formik>
					</Box>
				</Col>
			</Row>
		</Main>
	);
}
