import React from "react";
import { Link } from "react-router-dom";

import Main from "../../layout/Main";

import { Menu, MenuItem } from "./styles";
import UserIcon from "../../assets/img/target.svg";
import GroupIcon from "../../assets/img/teamwork.svg";

export default function Home() {
  return (
    <>
      <Main title="InterNations">
        <Menu>
          <MenuItem href="#">
            <Link to="/users">
              <div className="menu-icon">
                <img src={UserIcon} alt="Users" />
              </div>
              <p>
                Users
                <span>List of users registered in the system</span>
              </p>
            </Link>
          </MenuItem>

          <MenuItem href="#">
            <Link to="/groups">
              <div className="menu-icon">
                <img src={GroupIcon} alt="Groups" />
              </div>
              <p>
                Groups
                <span>Manage all the groups and its users</span>
              </p>
            </Link>
          </MenuItem>
        </Menu>
      </Main>
    </>
  );
}
