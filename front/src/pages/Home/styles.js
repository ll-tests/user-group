import styled from 'styled-components'

export const Container = styled.div `
  main {
    flex-direction: row;
  }
`

export const Menu = styled.div `
  position: relative;
  display: flex;
  flex: 1;
  flex-wrap: wrap;
  justify-content: space-evenly;
  text-decoration: none;

  @media (min-width: 770px) {
      justify-content: center;
      margin: 0;
      
      a:not(:first-of-type) {
        margin-left: 20px;
      }
  }
`

export const MenuItem = styled.div `
  @media (min-width: 770px) {
    &:not(:first-of-type) {
      margin-left: 20px;
    }
  }

  a {
    text-decoration: none;
    position: relative;
    display: flex;
    flex-direction: row;
    width: 100%;
    padding: 20px;
    background-color: #fff;
    color: #444;
    margin-bottom: 20px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.07);
    align-items: center;
    border-radius: 20px;

    @media (min-width: 770px) {
      flex-direction: column;
      width: 200px;
      transition: transform 0.5s, box-shadow 0.5s;

      &:hover,
      &:active {
        transform: translateY(-3px);
        box-shadow: 0px 4px 5px rgba(0,0,0,0.1);
      }

      .menu-icon {
        width: 100%;
        margin-right: 0px !important;
        margin-bottom: 20px;
        justify-content: center;
      }

      p {
        width: 100%;
        text-align: center;
      }
    }

    .menu-icon {
      display: flex;
      width: 70px;
      justify-content: flex-start;
      margin-right: 20px;

      img {
        width: 70px;
        height: 70px;
      }
    }

    p {
      display: flex;
      flex-direction: column;
      text-transform:uppercase;
      font-weight: bold;
      font-size: 18px;
    }

    span {
      font-weight: normal;
      text-transform: none;
      color: #aaa;
      font-size: 14px;
    }
  }
`