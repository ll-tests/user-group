import {
    createGlobalStyle
} from 'styled-components';

export const FormStyle = createGlobalStyle `

.input-error-msg {
    display: flex;
    align-items: center;
    padding-left: 10px;
    color: #a00;
}

  label {
      display: flex;
      flex-direction: column;
      justify-content: base;
      margin-bottom: 10px;

      small {
          align-items: flex-end;
          color: #aaa;
          display: flex;
          font-style: italic;
          font-size: 13px;
          margin-left: 10px;
          padding-bottom: 2px;
      }

      span {
          display: flex;
          margin-bottom: 5px;
      }
  }

  input[type=text] {
      align-items: center;
      border-radius: 20px;
      display: flex;
      font-size: 15px;
      height: auto;
      line-height: 30px;
  }

  .ant-select {
      display: flex;
      align-items: center;
    .ant-select-selection {
        padding-left: 10px;
        padding-top: 5px;
        border-radius: 20px;
    
        .ant-select-selection__placeholder {
            margin-left: -2px;
            font-size: 14px;
        }
    }
  }

  .form-actions {
      display: flex;
      margin: 20px 0 10px;
      justify-content: flex-end;
  }

  .ant-btn {
      border-radius: 20px;
  }
`;