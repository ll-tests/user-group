import styled from 'styled-components';

export const Container = styled.div `

  margin-bottom:20px;

  .ant-input {
    height: auto;
    line-height: 30px;
    border-radius: 20px;
  }
`;