import React from 'react';
import { Input } from 'antd';

import Box from '../Box';

import { Container } from './styles';

export default function Bar({ onSearch }) {
	const { Search } = Input;
	return (
		<Container>
			<Box>
				<Search onSearch={onSearch} allowClear />
			</Box>
		</Container>
	);
}
