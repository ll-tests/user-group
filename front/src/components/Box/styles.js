import styled from 'styled-components';

export const Container = styled.div `
    padding: 20px;
    background-color: #fff;
    border-radius: 20px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.07);
    transition: transform 0.2s, box-shadow 0.2s;
`;