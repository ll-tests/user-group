import styled from 'styled-components';

export const Container = styled.div `

    .ant-table-tbody > tr:hover:not(.ant-table-expanded-row):not(.ant-table-row-selected) > td {
        background: #fcfcfc;
    }

    .ant-table table {
        border-spacing: 0 20px !important;

        tbody {
            border-collapse: separate;
        }

        .ant-table-thead {
            display: none;
            height: 1px;
        }

        .ant-table-row {
            margin-bottom: 10px;
            cursor: pointer;
            
            td {
                background-color: #fff;
                
                &:hover {
                    transform: translateY(-3px);
                    box-shadow: 0px 4px 4px rgba(0,0,0,0.07);
                }

                &:first-child{
                    border-top-left-radius: 20px;
                    border-bottom-left-radius: 20px;
                }

                &:last-child{
                    border-top-right-radius: 20px;
                    border-bottom-right-radius: 20px;
                }
                
                div {
                    position: relative;
                    display: flex;
                    flex-direction: column;

                    .title {
                        font-weight: bold;
                        font-size: 18px;
                        margin: 0;
                    }

                    .subtitle {
                        color: #888;
                    }

                    .delete-link {
                        position: absolute;
                        cursor: pointer;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        width: 30px;
                        right: 0;
                        top: 0;
                        bottom: 0;
                        color: #a00;
                        background-color: transparent;
                        border: 0;

                        i {
                            border-radius: 20px;
                            transform: scale(1.4);
                            padding: 5px;
                            transition: background-color 0.5s, color 0.5s;

                            &:hover {
                                background-color: rgba(210,0,0,1);
                                color: #fff;
                            }
                        }
                    }
                }
            }
        }
    }
`;