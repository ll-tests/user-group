import React from 'react';
import { Table } from 'antd';

import { Container } from './styles';

export default function DataList({ data, columns, openLink = null, loading = false }) {
	return (
		<Container>
			<Table
				dataSource={data}
				loading={loading}
				columns={columns}
				rowKey={(record) => record.key}
				onRow={(record) => ({
					onClick: () => {
						openLink(record);
					}
				})}
			/>
		</Container>
	);
}
